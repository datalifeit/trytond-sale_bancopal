# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import party
from . import stock
from . import sale


def register():
    Pool.register(
        party.Party,
        party.BancopalPriceList,
        stock.Move,
        sale.Sale,
        module='sale_bancopal', type_='model')
