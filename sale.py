# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta

__all__ = ['Sale']


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def on_change_party(self):
        super(Sale, self).on_change_party()
        if self.party and self.party.bancopal and \
                self.party.bancopal_price_list:
            self.price_list = self.party.bancopal_price_list
