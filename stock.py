# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta

__all__ = ['Move']


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    def _get_context_sale_price(self):
        context = super(Move, self)._get_context_sale_price()
        if (self.shipment.customer.bancopal and
                self.shipment.customer.bancopal_price_list and
                self.shipment.__name__ == 'stock.shipment.out.return'):
            context['price_list'] = (self.shipment.customer.
                bancopal_price_list.id)
        return context
